/**
 * @file Template.cpp
 * @author Ana Jimena Lucas Cajahuaman
 * @brief File Exercise 19:
 				Dados como datos las coordenadas de los tres puntos P1, P2, P3 que corresponden a los v�rtices de un tri�ngulo, calcule su 
				per�metro y �rea.
 * @version 1.0
 * @date 28.01.2022
 */

/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include <math.h>
using namespace std;
/*******************************************************************************************************************************************
 *  												DEFINE
 *******************************************************************************************************************************************/
#define PI 3.1416
/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
float verticeAx=0.0;
float verticeAy=0.0;
float verticeBx=0.0;
float verticeBy=0.0;
float verticeCx=0.0;
float verticeCy=0.0;
double ladoAB=0;
double ladoBC=0;
double ladoAC=0;
double areaTriangulo=0;
double perimetroTriangulo=0;

/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Run();
void CollectData();
void Calculate();
void ShowResults();
double LongitudLado(float y1,float y2,float x1,float x2);
double AreaTriangulo(double ladoA, double ladoB, double ladoC);
double PerimetroTriangulo(double ladoA, double ladoB, double ladoC);

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/
int main(){	
	Run();
	return 0;
}
/*******************************************************************************************************************************************
*  												FUNCTION DEFINITION
*******************************************************************************************************************************************/
void Run(){
	CollectData();
	Calculate();
	ShowResults();
}
//=====================================================================================================
void CollectData(){
	cout<<"============Insert data============\r\n";
	cout<<"Ingrese los vertices del triangulo: ";
	cout<<"\n\tAx: ";
	cin>>verticeAx;
	cout<<"\tAy: ";
	cin>>verticeAy;
	cout<<"\n\tBx: ";
	cin>>verticeBx;
	cout<<"\tBy: ";
	cin>>verticeBy;
	cout<<"\n\tCx: ";
	cin>>verticeCx;
	cout<<"\tCy: ";
	cin>>verticeCy;
}
//=====================================================================================================
void Calculate(){
	ladoAB=LongitudLado(verticeAy,verticeBy,verticeAx,verticeBx);
	ladoBC=LongitudLado(verticeBy,verticeCy,verticeBx,verticeCx);
	ladoAC=LongitudLado(verticeAy,verticeCy,verticeAx,verticeCx);
	perimetroTriangulo=PerimetroTriangulo(ladoAB,ladoBC,ladoAC);
	areaTriangulo=AreaTriangulo(ladoAB,ladoBC,ladoAC);	
	}
//=====================================================================================================
void ShowResults(){
	cout<<"\r\n============Show result============\r\n";
	cout<<"El area del triangulo es: ";
	cout<<"\n\t"<<areaTriangulo;
	cout<<"\nEl perimetro del triangulo es: ";
	cout<<"\n\t"<<perimetroTriangulo;
}
//=====================================================================================================
double LongitudLado(float y1,float y2,float x1,float x2){
	double longitud=sqrt(pow((y1-y2),2)+pow((x1-x2),2));
	return longitud;
	
}
//=====================================================================================================
double AreaTriangulo(double ladoA, double ladoB, double ladoC){
	double a=perimetroTriangulo/2;
	double area=sqrt((a)*(a-ladoAB)*(a-ladoAC)*(a-ladoBC));
	return area;
}
//=====================================================================================================
double PerimetroTriangulo(double ladoA, double ladoB, double ladoC){
	double perimetro=ladoA+ladoB+ladoC;
	return perimetro;
}
//=====================================================================================================
