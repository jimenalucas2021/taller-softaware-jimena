/**
 * @file Template.cpp
 * @author Ana Jimena Lucas Cajahuaman
 * @brief File Exercise 03:
 			Queremos conocer los datos estad�sticos de una asignatura, por lo tanto, necesitamos un algoritmo que lea el n�mero de 
			desaprobados,aprobados, notables y sobresalientes de una asignatura, y nos devuelva:
				a. El tanto por ciento de alumnos que han superado la asignatura.
				b. El tanto por ciento de desaprobados, aprobados, notables y sobresalientes de la asignatura.
 * @version 1.0
 * @date 28.01.2022
 */

/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include<math.h>
using namespace std;
/*******************************************************************************************************************************************
 *  												DEFINE
 *******************************************************************************************************************************************/
#define PI 3.14 	
/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
int TotalStudent = 0;						//Statistical data
int InputDisapprovedStudents = 0;			//Entry of failed students
int InputApprovedStudents= 0;				//Entry of approved students
int InputNotableStudents= 0;				//Notable student entry
int InputOutstandingStudent= 0;				//Outstanding Students Entry
float ExceededPercentage= 0.0;				//Percentage of those who passed the course				
float DisapprovedPercentage= 0.0;			//Percentage of those who failed the course
float ApprovedPercentage= 0.0;				//Percentage of those who passed the course
float NotablesPorcentage= 0.0;				//Percentage of notables in the course
float OutstandingePercentage= 0.0;			//Percentage of the outstanding of the course
/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Run();
void CollectData();
void Calculate();
void ShowResults();
int CalculateTotalStudent(int disapproved, int approved, int notable, int outstanding);
float CalculateStudentsPassCourse(int total, int approved, int notable, int outstanding);
float CalculateFailedPercentageCourse(int total, int disapproved);
float CalculatePassedPercentageCourse(int total, int approved);
float CalculateNotablesPercentageCourse(int total, int notable);
float CalculateOutstandingPercentageCourse(int total, int outstanding);
/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/
int main(){
	Run();
 	return 0;
}
 /*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/
void Run(){
	CollectData();
	Calculate();
	ShowResults();
}
//=====================================================================================================

void CollectData(){	
 	cout<<"============Insert data============\r\n";
 	cout<<"Calculation in a course\r\n";
 	cout<<"\tEnter the number of disapproved: ";
 	cin>>InputDisapprovedStudents;
 	cout<<"\tEnter the number of approved students: ";
 	cin>>InputApprovedStudents;
 	cout<<"\tEnter the number of notable students: ";
 	cin>>InputNotableStudents;
 	cout<<"\tEnter the number of outstanding students: ";
 	cin>>InputOutstandingStudent;
}
//=====================================================================================================
void Calculate(){
	TotalStudent = CalculateTotalStudent(InputDisapprovedStudents,InputApprovedStudents,InputNotableStudents,InputOutstandingStudent);
	StudentsPassCourse = CalculateStudentsPassCourse(TotalStudent ,InputApprovedStudents ,NotablesPorcentage ,OutstandingePercentage);
	FailedPercentageCourse = CalculateFailedPercentageCourse(TotalStudent,InputDisapprovedStudents);
	ApprovedPercentage = CalculateStudentsPassCourse(TotalStudent,InputApprovedStudents);
	NotablesPorcentage = CalculateNotablesPercentageCourse(TotalStudent,InputNotableStudents);
	OutstandingePercentage = CalculateOutstandingPercentageCourse(TotalStudent,OutstandingePercentage);
}
//=====================================================================================================
void ShowResults(){
 	cout<<"============Show result============\r\n";
 	cout<<"El total de estudiantes es:"<<TotalStudent<<"\r\n";
 	cout<<"\tEl procentaje de quienes pasaron el curso: "<<StudentsPassCourse<<"%\r\n";
 	cout<<"\tEl porcentaje de quienes desaprobaron el curso: "<<FailedPercentageCourse<<"%\r\n";
 	cout<<"\tEl procentaje de quienes aprobaron el curso:"<<ApprovedPercentage<<"%\r\n";
 	cout<<"\tEl porcentaje de quienes son notables del curso: "<<NotablesPorcentage<<"%\r\n";
 	cout<<"\tEl porcentaje de quienes son sobresalientes del curso: "<<OutstandingePercentage<<"%\r\n";
}
//=====================================================================================================
int CalculateTotalStudent(int disapproved, int approved, int notable, int outstanding){
	return disapproved+approved+notable+outstanding;
}
//=====================================================================================================
float CalculateStudentsPassCourse(int total, int approved, int notable, int outstanding){
	return ((float)approved+(float)notable+(float)outstanding)*100.0/total;
}
//=====================================================================================================
float CalculateFailedPercentageCourse(int total, int disapproved){
	return ((float)disapproved)*100.0/total;
}
//=====================================================================================================
float CalculatePassedPercentageCourse(int total, int approved){
	return ((float)approved)*100.0/total;
}
//=====================================================================================================
float CalculateNotablesPercentageCourse(int total, int notable){
	return ((float)notable)*100.0/total;
}
//=====================================================================================================
float CalculateOutstandingPercentageCourse(int total, int outstanding){
	return ((float)outstanding)*100.0/total;
}
//=====================================================================================================
