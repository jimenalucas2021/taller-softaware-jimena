/**
 * @file Template.cpp
 * @author Ana Jimena Lucas Cajahuaman
 * @brief File Exercise 17:
 				El �rea de una elipse se obtiene con la f�rmula ?ab , donde a es el radio menor de la elipse y b es el radio mayor, y su 
 				per�metro se obtiene con la f�rmula ?[4(a+b)2]0.5. Realice un programa en C ++ utilizando estas f�rmulas y calcule el �rea 
		    	y el per�metro de una elipse que tiene un radio menor de 2.5 cm y un radio mayor de 6.4 cm.
 * @version 1.0
 * @date 28.01.2022
 */

/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include <math.h>
using namespace std;
/*******************************************************************************************************************************************
 *  												DEFINE
 *******************************************************************************************************************************************/
#define PI 3.1416								//Constante Pi
/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
float terminalMinorRadius = 0.0;				//Entrada de radio menor a traves del teclado
float terminalMajorRadius = 0.0;				//Entarda de radio mayor a traves del teclado

float area = 0.0;
float average = 0.0;
/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Run();
void CollectData();
void Calculate();
void ShowResults();

float CalculateArea(float MinorRadius, float MajorRadius);
float CalculateAverage(float MinorRadius, float MajorRadius);
/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/
int main(){
	Run();
	return 0;
}
/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/
void Run(){
	CollectData();
	Calculate();
	ShowResults();
}
//=====================================================================================================
void CollectData(){	
	cout<<"========= Insert Data ===========\r\n";
	cout<<"\tWrite the minor radius: ";
	cin>>terminalMinorRadius;
	cout<<"\tWrite the major radius: ";
	cin>>terminalMajorRadius;
}
//=====================================================================================================
void Calculate(){
	area = CalculateArea(terminalMinorRadius,terminalMajorRadius);
	average = CalculateAverage(terminalMinorRadius,terminalMajorRadius);
}
//=====================================================================================================
void ShowResults(){
	cout<<"\r\n========= Show Results ===========\r\n";
	cout<<"\tThe area is: "<<area<<"\r\n";
	cout<<"\tThe average is: "<<average<<"\r\n";
}
//=====================================================================================================
float CalculateArea(float MinorRadius, float MajorRadius){
	return PI * MinorRadius * MajorRadius; 								//area = pi*radio menor*radio mayor
}
//=====================================================================================================
float CalculateAverage(float MinorRadius, float MajorRadius){
	return 2.0*PI*sqrt((pow(MinorRadius,2) + pow(MajorRadius,2))/2.0);	//perimetro
}
//=====================================================================================================
