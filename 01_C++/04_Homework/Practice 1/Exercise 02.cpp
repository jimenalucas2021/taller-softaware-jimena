/**
 * @file Template.cpp
 * @author Ana Jimena Lucas Cajahuaman
 * @brief File Exercise 02:
 			Un maestro desea saber qu� porcentaje de hombres y que porcentaje de mujeres hay en un grupo de estudiantes. 	
 * @version 1.0
 * @date 28.01.2022
 */

/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include<math.h>
using namespace std;
/*******************************************************************************************************************************************
 *  												DEFINE
 *******************************************************************************************************************************************/
#define PI 3.14 	
/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
float InputManQuantity = 0.0; 		//Entry of the number of men in the group
float InputWomanQuantity = 0.0; 	//Entry of the number of women in the group
double PercentageMen = 0; 			//Percentage of men in the group
double PercentageWoman = 0;			//Percentage of women in the group
/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Run();
void CollectData();
void Calculate();
void ShowResults();
double CalculatePercentage(float number);
/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/
int main(){
	Run();
	return 0;
}
/*******************************************************************************************************************************************
*  												FUNCTION DEFINITION
*******************************************************************************************************************************************/
void Run(){
	CollectData();
	Calculate();
	ShowResults();
}
//=====================================================================================================
void CollectData(){
	cout<<"============Insert data============\r\n";
	cout<<"Enter the number of women in the group: ";
	cin>>InputWomanQuantity;
	cout<<"Enter the number of men in the group: ";
	cin>>InputManQuantity;
}
//=====================================================================================================
void Calculate(){
	PercentageMen = CalculatePercentage(InputManQuantity);
	PercentageWoman = CalculatePercentage(InputWomanQuantity);
}
//=====================================================================================================
void ShowResults(){
	cout<<"\r\n============Show result============\r\n";
	cout<<"\n\tThe percentage of women in the group is: ";
	cout<<"\n\t"<<PercentageWoman<<"%";
	cout<<"\n\tThe percentage of men in the group is: ";
	cout<<"\n\t"<<PercentageMen<<"%";
}
//=====================================================================================================
double CalculatePercentage(float number){
	double total=InputManQuantity+InputWomanQuantity;
	return number*100/total; //Regresa el porcentaje de la cantidad requerida
}
//=====================================================================================================
