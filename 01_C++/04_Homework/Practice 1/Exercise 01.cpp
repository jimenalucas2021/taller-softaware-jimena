/**
 * @file Template.cpp
 * @author Ana Jimena Lucas Cajahuaman
 * @brief File Exercise 01:
 			Elabore un algoritmo que dados como datos  de entrada el radio y la altura de un cilindro calcular, el rea lateral 
			y el volumen del cilindro.	
 * @version 1.0
 * @date 28.01.2022
 */

/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include<math.h>
using namespace std;
/*******************************************************************************************************************************************
 *  												DEFINE
 *******************************************************************************************************************************************/
#define PI 3.14 	
/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
float inputRadius = 0.0;			//Radius entered via keyboard
float inputHeight = 0.0;			//Height entered via keyboard 
	
float resultCircularArea = 0.0; 	//circular area result
float resultLateralArea = 0.0;		//lateral area result
float resultCylinderVolume = 0.0;	//volume result
/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Run();
void CollectData();
void Calculate();
void ShowResults();

float CalculateCircularArea(float radius);
float CalculateLateralArea(float radius, float height); 
float CalculaVolume(float radius, float height);
/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/
int main(){
	Run();
	return 0;
}
/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/
void Run(){
	CollectData();	
	Calculate();
	ShowResults();
}
//===============================================================================================================================================
void CollectData(void){
	cout<<"========= Insert Data ===========\r\n";
	cout<<"Enter the radius of the cylinder in meters: ";
	cin>>inputRadius;
	cout<<"Enter the heigth of the cylinder in meters: ";
	cin>>inputHeight;
}
//=====================================================================================================
void Calculate(){
	resultCircularArea = CalculateCircularArea(inputRadius);
	resultLateralArea = CalculateLateralArea(inputRadius, inputHeight);
	resultCylinderVolume = CalculaVolume(inputRadius, inputHeight);
}
//=====================================================================================================
void ShowResults(){
	cout<<"\r\n========= Show Results ===========\r\n";
	cout<<"\tThe circular area is: "<<resultCircularArea<<" m.\n\r";
	cout<<"\tThe lateral area is: "<<resultLateralArea<<" m.\n\r";
	cout<<"\tThe volume is: "<<resultCylinderVolume<<" m.\n\r";
}
//=====================================================================================================
float CalculateCircularArea(float radius){
	return PI*pow(radius,2);
}
//=====================================================================================================
float CalculateLateralArea(float radius, float height){
	return 2.0*PI*radius*height;	
}
//=====================================================================================================
float CalculaVolume(float radius, float height){
	return PI*pow(radius,2)*height;
}
