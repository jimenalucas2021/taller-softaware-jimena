/**
 * @file Template.cpp
 * @author Ana Jimena Lucas Cajahuaman
 * @brief File Exercise 18:
 				Si coloca una escalera de 3 metros a un �ngulo de 85 grados al lado de un edificio, la altura en la cual la escalera toca el 
				edificio se puede calcular como altura=3 * seno 85�. Calcule esta altura con una calculadora y luego escriba un programa en C 
				que obtenga y visualice el valor de la altura. 
 * @version 1.0
 * @date 28.01.2022
 */

/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include<iostream>
#include<math.h>
using namespace std;
/*******************************************************************************************************************************************
 *  												DEFINE
 *******************************************************************************************************************************************/
#define PI 3.1416
/*******************************************************************************************************************************************
 *  											GLOBAL VARIABLES
 *******************************************************************************************************************************************/
float stairMeasure = 3.0;
float angle = 85.0;

float angleToRadians = 0.0;
float height = 0.0;
/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Run();
void CollectData();
void Calculate();
void ShowResults();
float ConvertToRadians(float angle);
float CalculateTheHeight(float stairMeasure,float angleToRadians);
/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/
int main(){
	Run();
	
	return 0;
}
/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/
void Run(){
	CollectData();
	Calculate();
	ShowResults();
}
//=====================================================================================================
void CollectData(){
	cout<<"============Insert data============\r\n";
	cout<<"Calculation in a course\r\n";
	cout<<"The size of the ladder is "<<stairMeasure<<" m.\r\n";
	cout<<"With the angle of "<<angle<<"sexagesimal degrees\r\n"; 
}
//=====================================================================================================
void Calculate(){
	angleToRadians = ConvertToRadians(angle);
	height = CalculateTheHeight(stairMeasure,angleToRadians);
}
//=====================================================================================================
void ShowResults(){
	cout<<"============Show result============\r\n";
	cout<<"The height is: "<<height<<" m.\r\n";
}
//=====================================================================================================
float ConvertToRadians(float angle){
	return (PI * angle)/180.0;
}
//=====================================================================================================
float CalculateTheHeight(float stairMeasure,float angleToRadians){
	return stairMeasure * sin(angleToRadians);
}

