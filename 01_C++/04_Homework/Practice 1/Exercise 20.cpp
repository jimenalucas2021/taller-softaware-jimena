/**
 * @file Exercise 20.cpp
 * @author Oscar Lopez (oscar.lopez10@unmsm.edu.pe)
 * @brief Ingresar los lados de un triangulo y el �ngulo que forman, 
 		  e imprima el  valor del tercer lado,  los otros dos �ngulos y el �rea del tri�ngulo
 * @version 1.7
 * @date 28.01.2022
 * 
 */

/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include<iostream>
#include<math.h>
using namespace std;

/*******************************************************************************************************************************************
 *  												DEFINE
 *******************************************************************************************************************************************/
#define PI 3.141592

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
float inputSide1 = 0.0;
float inputSide2 = 0.0;;
float inputAngle1 = 0.0;

float angle1Converted = 0.0;	//La variable inputAngle1 transformado a radianes
float side3 = 0.0;
float triangleArea = 0.0;
float angle2 = 0.0;				//Angulo 2 en radianes
float angle3 = 0.0; 			//Angulo 3 en radianes
float angle2Converted = 0.0;
float angle3Converted = 0.0;

/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Run();
void CollectData();
void Calculate();
void ShowResults();
float ConvertToRadians(float angle);
float ConvertToSexagesimals(float angle);
float CalculateSide(float side1,float side2,float angle);
float CalculateAngle(float side1,float side2,float oppositeSide);
float CalculateArea(float side1,float side2,float side3);

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	Run();
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/
void Run(){
	CollectData();
	Calculate();
	ShowResults();
}
//=====================================================================================================
void CollectData(){
	cout<<"============Insert data============\r\n";
	cout<<"\tCalculation in a course\r\n";
	cout<<"\tType the first side: ";
	cin>>inputSide1;
	cout<<"\tType the second side: ";
	cin>>inputSide2;
	cout<<"\tEnter the angle between the two sides: ";
	cin>>inputAngle1;
}
//=====================================================================================================
void Calculate(){
	angle1Converted = ConvertToRadians(inputAngle1);
	side3 = CalculateSide(inputSide1,inputSide2,angle1Converted);
	angle2 = CalculateAngle(inputSide2,side3,inputSide1);//Reutilizable
	angle3 = CalculateAngle(side3,inputSide1,inputSide2);
	angle2Converted = ConvertToSexagesimals(angle2);//Reutilizable
	angle3Converted = ConvertToSexagesimals(angle3);
	triangleArea =  CalculateArea(inputSide1,inputSide2,side3);
}
//=====================================================================================================
void ShowResults(){
	cout<<"============Show result============\r\n";
	cout<<"\tThe measure of the third side is: "<<side3<<" u.\r\n";
	cout<<"\tThe measure of the first angle is: "<<angle2<<" radians or "<<angle2Converted<<" sexagesimals\r\n";
	cout<<"\tThe measure of the second angle is: "<<angle3<<" radians or "<<angle3Converted<<" sexagesimals\r\n";
	cout<<"\tThe area of the triangle is: "<<triangleArea<<" square units";
}
//=====================================================================================================
float ConvertToRadians(float angle){
	return PI * angle / 180.0;
}
//=====================================================================================================
float CalculateSide(float side1,float side2,float angle){
	return sqrt(pow(side1,2) + pow(side2,2) - 2.0*side1*side2*cos(angle));
}
//=====================================================================================================
float CalculateAngle(float side1,float side2,float oppositeSide){
	float angle = 0.0;
	
	angle = acos((pow(side1,2)+ pow(side2,2)-pow(oppositeSide,2))/(2.0*side1*side2)); //Derivado de la ley de cosenos
	
	return angle;
}
//=====================================================================================================
float ConvertToSexagesimals(float angle){
	return angle*180.0/PI;
}
//=====================================================================================================
float CalculateArea(float side1,float side2,float side3){
	float semiperimeter = 0.0;
	float area = 0.0;
	
	semiperimeter = (side1 + side2 + side3)/2.0;
	area = sqrt(semiperimeter*(semiperimeter-side1)*(semiperimeter-side2)*(semiperimeter-side3));
	
	return area;
}
