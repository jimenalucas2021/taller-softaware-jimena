/**
 * @file Template.cpp
 * @author Ana Jimena Lucas Cajahuaman
 * @brief File Exercise 01:
			Calcular el pago semanal de un trabajador. Los datos a ingresar son: Total de horas trabajadas y el pago por hora.
			Si el total de horas trabajadas es mayor a 40 la diferencia se considera como horas extras y se paga un 50% mas que una hora normal.
			Si el sueldo bruto es mayor a s/. 500.00, se descuenta un 10% en caso contrario el descuento es 0.
 * @version 1.0
 * @date 05.02.2022
 */

/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
using namespace std;
/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
float InputTotalHoursWorked = 0.0;		//revenue from total hours worked
float InputHourly = 0.0;				//paid income for each hour worked

float SalaryExtraHours = 0.0; 
float SalaryNormalHours = 0.0; 
float FirstTotalSalary = 0.0; 
float ExtraHours = 0.0; 
float TotalSalary = 0.0;	
float FinalTotalSalary = 0.0;
/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Run();
void CollectData();
void Calculate();
void ShowResults();

float  CalculateFinalTotalSalary(float InputTotalHoursWorked,float InputHourly);
/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/
int main(){
	Run();
	return 0;
}
/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/
void Run(){
	CollectData();	
	Calculate();
	ShowResults();
}
//===============================================================================================================================================
void CollectData(void){
	cout<<"========= Insert Data ===========\r\n";
	cout<<"\tEnter the number of total hours worked: ";
	cin>>InputTotalHoursWorked;
	cout<<"\tWrite how much you get paid per hour: ";
	cin>>InputHourly;
}
//=====================================================================================================
void Calculate(){
	TotalSalary = CalculateFinalTotalSalary(InputTotalHoursWorked,InputHourly);
}
//=====================================================================================================
void ShowResults(){
	cout<<"\r\n========= Show Results ===========\r\n";
	cout<<"\tThe payment is: $"<<TotalSalary<<" .\n\r";
}
//=====================================================================================================
float CalculateFinalTotalSalary(float InputTotalHoursWorked,float InputHourly){
	if (InputTotalHoursWorked > 40){
		ExtraHours = InputTotalHoursWorked - 40;
	}
	SalaryExtraHours = ExtraHours * (0.5*InputHourly);
	SalaryNormalHours = InputHourly * InputTotalHoursWorked;
	FirstTotalSalary = SalaryNormalHours + SalaryExtraHours;
	
	if (FirstTotalSalary > 500){
		FinalTotalSalary = FirstTotalSalary - (0.1*FirstTotalSalary);
	}
	else {
		FinalTotalSalary = FirstTotalSalary;
	}
	
	return FinalTotalSalary;
}
//=====================================================================================================

