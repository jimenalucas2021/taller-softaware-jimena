/**
 * @file Template.cpp
 * @author Ana Jimena Lucas Cajahuaman
 * @brief File Exercise 02:
			A un trabajador le descuentan de su sueldo el 10% si su sueldo es menor o igual a 1000, 
			por encima de 1000 hasta 2000 el 5% del adicional, y por encima de 2000 el 3% del 
			adicional. Calcular el descuento y sueldo neto que recibe el trabajador dado un sueldo,

 * @version 1.0
 * @date 28.02.2022
 */

/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
using namespace std;

/*******************************************************************************************************************************************
 *  												DEFINE
 *******************************************************************************************************************************************/


/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
float globalTerminalSalary = 0.0;		// Entrada del salario a travs del terminal

float totalDiscount = 0.0;				// Descuento total
float totalNetSalary = 0.0;				// Salario neto

/*******************************************************************************************************************************************
 *  												TYPE DEFINITION
 *******************************************************************************************************************************************/
typedef enum {
	Success,
	Error,
	ERROR_TERMINAL,
	ERROR_CONVERSION
} Result;

/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
bool Run();
Result CollectData();
int CheckData();
void ShowResults();
void Calculate();
float DiscountSelection(float salary);
float NetSalary(float salary, float discount);

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/
int main(){
	
	Run();		
	
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/
bool Run(){
	
	while(true){
		
		if (CollectData() == Error){
			cout<<"\n\t! CollectData Error !\n";
            cout<<"\n\tPlease write a correct data.\n\n";
            continue;
		}
		Calculate();
		ShowResults();
		break;
	}
	
	return true;
}
//=====================================================================================================
Result CollectData(){	
	cout<<"========= Insert Data ===========\r\n";
	cout<<"\tWrite the salary: ";
	globalTerminalSalary = CheckData();
	if(globalTerminalSalary == -1){
		return Error;
	}
	
	return Success;
}

//=====================================================================================================
int CheckData(){
	int data = 0;
	
	while(true){
		cin>>data;
        if(cin.fail() == true){
        	cin.clear();
            cin.ignore(1000,'\n');
            return -1;
			break;	
        } else { 
			if (data > 0){ 
			    return data;
				break;
			} else {
          	  	return -1;
			}	
		}
    }
}

//=====================================================================================================
void Calculate(){
	totalDiscount = DiscountSelection(globalTerminalSalary);
	totalNetSalary = NetSalary(globalTerminalSalary,totalDiscount);
}

//=====================================================================================================
void ShowResults(){
	cout<<"\r\n========= Show Results ===========\r\n";
	cout<<"\tThe discount is : "<<"S/."<<totalDiscount<<"\r\n";
	cout<<"\tThe net salary is: "<<"S/."<<totalNetSalary<<"\r\n";
}

//=====================================================================================================
float DiscountSelection(float salary){
	if (salary <= 1000){
		return 0.1 * salary;
	} else {
		if (salary<=2000){
			return 0.05 * (salary-1000);
		} else {
			return 0.03 * (salary-2000);
		}
	}
}

//=====================================================================================================
float NetSalary(float salary, float discount){
	return salary - discount;
}

//=====================================================================================================

