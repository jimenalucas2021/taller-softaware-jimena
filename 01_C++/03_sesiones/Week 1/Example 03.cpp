#include <iostream>

using namespace std;

/*
Funcion ejemplo 03
*/
int main(){
	
	//Declaration of example 03
	int numberInput;
	int resultSum;
	int resultRest;
	
	//Initialize
	numberInput = 0;
	resultSum = 0;
	resultRest = 0;
	
	//Display phare 1
	cout <<"Escribe un numero entero :";
	cin>>numberInput;
	
	//operador suma
	resultSum = numberInput + 2;
	resultRest = numberInput - 1;
		
	//Display phrase 2 with number
	cout<<"el resultado de la suma es :"<<resultSum;
	cout<<"\n\rel resultado de la resta es :"<<resultRest;
	
	return 0;
}


